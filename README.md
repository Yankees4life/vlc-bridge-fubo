# vlc-bridge-fubo



## Docker

```
docker run -d -e 'FUBO_USER=<username>' -e 'FUBO_PASS=<password>' -p 7777:7777 -v config_dir:/app/Config --name vlc-bridge-fubo registry.gitlab.com/yankees4life/vlc-bridge-fubo:latest
```

## Native

```
git clone https://gitlab.com/Yankees4life/vlc-bridge-fubo
cd vlc-bridge-fubo
pip3 install -r requirements.txt
python3 pywsgi.py
```